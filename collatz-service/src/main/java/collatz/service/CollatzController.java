package collatz.service;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Single;

import java.math.BigInteger;

@Controller("/")
public class CollatzController {


    @Get("/3/{bi}")
    Flowable<BigInteger> calculate(final BigInteger bi){
        System.out.println("collatz "+bi);
        return Flowable.create(emitter -> {
            int count = 0;
            BigInteger n = bi;
            while (!n.equals(BigInteger.ONE)) {
                if (n.mod(new BigInteger("2")) == BigInteger.ZERO) {
                    n = n.divide(new BigInteger("2"));
                } else {
                    n = n.multiply(new BigInteger("3"));
                    n = n.add(new BigInteger("1"));
                }
                emitter.onNext(n);
                count++;
            }
            emitter.onComplete();
        }, BackpressureStrategy.BUFFER);
    }

}
