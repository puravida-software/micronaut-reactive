package api.service.sync.backend;

import io.micronaut.http.annotation.Get;
import io.micronaut.http.client.annotation.Client;
import io.reactivex.Flowable;

import java.math.BigInteger;
import java.util.List;

@Client("collatz-service")
public interface CollatzService {

    @Get("/3/{bi}")
    List<BigInteger> calculate(BigInteger bi);

}
