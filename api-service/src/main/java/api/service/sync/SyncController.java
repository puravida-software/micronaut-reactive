package api.service.sync;

import api.service.sync.backend.AuthService;
import api.service.sync.backend.CollatzService;
import api.service.sync.backend.EchoService;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;

import javax.inject.Inject;
import java.math.BigInteger;
import java.util.*;

@Controller("/sync")
public class SyncController {

    @Inject
    AuthService authService;

    @Inject
    EchoService echoService;

    @Inject
    CollatzService collatzService;

    @Post
    Map<String,Object> run(String username, String password){
        System.out.println(System.currentTimeMillis());
        Map<String, Object> ret = new HashMap<>();
        Map<String, String> login =authService.login(username, password);
        ret.putAll(login);

        List<BigInteger> collatzs = collatzService.calculate(new BigInteger(""+Math.abs(new Random().nextLong())));
        ret.put("collatz", collatzs.size());

        String reverse = echoService.reverse(login.get("username"));
        ret.put("reverse", reverse);

        return ret;
    }


}
