package api.service.reactive;

import api.service.reactive.backend.AuthService;
import api.service.reactive.backend.CollatzService;
import api.service.reactive.backend.EchoService;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;

import javax.inject.Inject;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Controller("/reactive")
public class ReactiveController {

    @Inject
    AuthService authService;

    @Inject
    EchoService echoService;

    @Inject
    CollatzService collatzService;

    @Post
    Single<Map<String, Object>> run(final String username, final String password) {
        System.out.println(System.currentTimeMillis());
        return Single.create(emitter -> {
            final Map<String, Object> ret = new HashMap<>();
            authService.login(username, password).subscribe(login -> {

                if (login.status() != HttpStatus.OK) {
                    emitter.onError(new RuntimeException("Error at login"));
                    return;
                }

                ret.putAll(login.body());


                Single<Long> count = collatzService.calculate(new BigInteger("" + Math.abs(new Random().nextLong()))).count();

                Maybe<String> reverse = echoService.reverse(ret.get("username").toString());

                count.subscribe(
                        c -> {
                            ret.put("collatz", c);
                            reverse.subscribe(
                                    s -> {
                                        ret.put("reverse", s);
                                        emitter.onSuccess(ret);
                                    },
                                    err -> emitter.onError(err)
                            );
                        },
                        err ->
                                emitter.onError(err)
                );

            });
        });
    }
}
