package api.service.reactive.backend;

import io.micronaut.http.annotation.Get;
import io.micronaut.http.client.annotation.Client;
import io.reactivex.Maybe;

@Client("echo-service")
public interface EchoService {

    @Get("/{word}")
    Maybe<String> reverse(String word);

}
