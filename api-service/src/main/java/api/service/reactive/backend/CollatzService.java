package api.service.reactive.backend;

import io.micronaut.http.annotation.Get;
import io.micronaut.http.client.annotation.Client;
import io.reactivex.Flowable;

import java.math.BigInteger;

@Client("collatz-service")
public interface CollatzService {

    @Get("/3/{bi}")
    Flowable<BigInteger> calculate(BigInteger bi);

}
