package api.service.reactive.backend;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.client.annotation.Client;
import io.reactivex.Single;

import java.util.Map;

@Client("auth-service")
public interface AuthService {

    @Post
    Single<HttpResponse<Map<String,String>>> login(String username, String password);

}
