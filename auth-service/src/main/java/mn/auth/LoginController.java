package mn.auth;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import io.reactivex.Single;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

@Controller("/")
public class LoginController {

    @Post
    Single<HttpResponse<UserBean>>login( String username, String password){
        System.out.println(System.currentTimeMillis());
        return Single.create( emitter->{
            HttpResponse<UserBean> ret;
            UserBean user = new UserBean();
            user.username = username;
            user.token = UUID.randomUUID().toString();
            ret = HttpResponse.ok(user);
            emitter.onSuccess(ret);
        });
    }


}
