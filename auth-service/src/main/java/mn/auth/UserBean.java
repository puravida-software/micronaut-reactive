package mn.auth;

import io.micronaut.core.annotation.Introspected;

@Introspected
public class UserBean {

    public String username;
    public String token;

}
