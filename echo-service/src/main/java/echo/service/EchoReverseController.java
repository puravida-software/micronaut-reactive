package echo.service;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.reactivex.Maybe;
import io.reactivex.Observable;

import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Controller("/")
public class EchoReverseController {

    @Get("/{word}")
    Maybe<String> reverse(final String word) {
        return Maybe.create(emitter -> {
            Observable
                    .defer(() -> {
                        if ((System.currentTimeMillis() % 2) == 0) {
                            System.out.println("Vamos a simular una exception de negocio");
                            throw new RuntimeException("Milis es par");
                        }

                        List<Character> list = word.chars().mapToObj(c -> (char) c)
                                .map(Character::toUpperCase)
                                .collect(Collectors.toList())
                                .stream()
                                .sorted()
                                .collect(Collectors.toList());

                        return Observable.just(
                                list
                                .stream()
                                .map(c -> c.toString())
                                .collect(Collectors.joining()));
                    })
                    .retryWhen(
                            f -> f.take(3).delay(new Random().nextInt(8), TimeUnit.SECONDS))
                    .subscribe(
                            str -> emitter.onSuccess(str),
                            err -> emitter.onError(err)
                    );
        });
    }


}
