USERNAME ?= jagedn
PASSWORD ?= jagedn
USERS ?= 2
TIMEOUT ?= 1m

simple:
	siege -c ${USERS} -t ${TIMEOUT}  --content-type "application/json" 'http://localhost:8080/sync POST {"username":"${USERNAME}","password":"${PASSWORD}"}'

reactive:
	siege -c ${USERS} -t ${TIMEOUT}  --content-type "application/json" 'http://localhost:8080/reactive POST {"username":"${USERNAME}","password":"${PASSWORD}"}'
